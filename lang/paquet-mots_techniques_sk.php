<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mots_techniques?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// M
	'mots_techniques_description' => 'Podľa predvolených nastavení cykly MOTS a GROUPES_MOTS vyfiltrujú technické kľúčové slová. Kritérium <code>{tout}</code> však vypíše kľúčové slová a kritérium <code>{technique=oui}</code> 
				umožní vybrať technické podľa typu.',
	'mots_techniques_slogan' => 'Skupine kľúčových slov pridá spresnenie "technické"',
	'mots_techniques_titre' => 'Technické kľúčové slová',
];
