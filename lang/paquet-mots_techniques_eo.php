<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mots_techniques?lang_cible=eo
// ** ne pas modifier le fichier **

return [

	// M
	'mots_techniques_description' => 'Defaŭlte, la iteracioj MOTS („vortoj“) kaj GROUPES_MOTS („grupoj_vortoj“) filtros la teknikajn ŝlosilvortojn. Tamen, per la kriterio <code>{tout}</code> („ĉio“), oni ricevos ĉiujn ŝlosilvortojn, kaj per la kriterio <code>{technique=oui}</code> („teknika=jes“), oni ricevos nur la teknikajn ŝlosilvortojn.',
	'mots_techniques_slogan' => 'Aldonas „teknikan“ specialaĵon al la grupoj de ŝlosilvortoj.',
	'mots_techniques_titre' => 'Teknikaj vortoj',
];
