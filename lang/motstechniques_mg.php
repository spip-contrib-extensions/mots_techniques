<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/motstechniques?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_mots_cles_techniques' => 'Les mots-clés de ce groupe seront cachés par défaut dans l’espace public ?',

	// I
	'info_mots_cles_techniques' => 'Groupe de mots-clés techniques : ',
];
