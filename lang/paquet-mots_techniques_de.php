<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mots_techniques?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// M
	'mots_techniques_description' => 'In der Grundeinstellung verbergen die Schleifen MOTS und GROUPES_MOTS die technischen Schlagworte.
Der Filter <code>{tout}</code> erlaubt dennoch alle Schagworte zurückzugeben, und de Filter <code>{technique=oui}</code> ermöglicht eine Auswahl nach technischem Typ.',
	'mots_techniques_slogan' => 'Fügt eine spezialisierte « technische » Schagwortgruppe hinzu.',
	'mots_techniques_titre' => 'Technische Schlagworte',
];
