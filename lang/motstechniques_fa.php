<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/motstechniques?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_mots_cles_techniques' => 'آيا كليد‌واژه‌هاي اين گروه به صورت پيش‌گزيده در فضاي همگاني پنهان شود؟‌',

	// I
	'info_mots_cles_techniques' => 'فن‌هاي گروه كليد واژه:',
];
