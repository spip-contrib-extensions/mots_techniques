<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mots_techniques?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// M
	'mots_techniques_description' => 'By default, the loops MOTS and GROUPES_MOTS keywords will filter technical keywords. However, the criterion <code>{tout}</code> will return all
keywords and the criterion <code>{technique=oui}</code> allows to select by technical type.',
	'mots_techniques_slogan' => 'Adds a "technical" specialization to the keywords groups',
	'mots_techniques_titre' => 'Technical keywords',
];
