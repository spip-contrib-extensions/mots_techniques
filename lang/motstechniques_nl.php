<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/motstechniques?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_mots_cles_techniques' => 'Moeten technische trefwoorden standaard verborgen worden op de publieke site?',

	// I
	'info_mots_cles_techniques' => 'Groep van technische trefwoorden: ',
];
