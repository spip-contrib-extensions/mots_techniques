<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/motstechniques?lang_cible=eo
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_mots_cles_techniques' => 'La ŝlosilvortoj de tiu grupo estos kaŝitaj defaŭlte en la publika spaco ?',

	// I
	'info_mots_cles_techniques' => 'Grupo de teknikaj ŝlosilvortoj :',
];
