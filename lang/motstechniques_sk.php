<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/motstechniques?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_mots_cles_techniques' => 'Kľúčové slová z tejto skupiny budú podľa predvolených nastavení na verejne prístupnej stránke skryté?',

	// I
	'info_mots_cles_techniques' => 'Skupina technických kľúčových slov: ',
];
