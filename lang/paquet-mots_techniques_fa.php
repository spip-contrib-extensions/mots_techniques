<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mots_techniques?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// M
	'mots_techniques_description' => 'پيش‌گزيده، حقله‌هاي كليد‌واژه‌هاي MOTS و  GROUPES_MOTS  كليدواژه‌هاي فني را فيلتر خواهند كرد. با اين حال، معيار<code>{tout}</code> تمام كليدواژه‌ها را برخواهد گرداند و معيار <code>{technique=oui}</code> اجازاه‌ي انتخاب نوع فني را خواهد داد. 
',
	'mots_techniques_slogan' => 'افزودن «فن» ويژه‌سازي به يك گروه از كليدواژه‌ها.',
	'mots_techniques_titre' => 'واژه‌هاي فني ',
];
