<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mots_techniques?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// M
	'mots_techniques_description' => 'Par défaut, les boucles MOTS et GROUPES_MOTS filtreront les mots-clés 
				techniques. Cependant, le critère <code>{tout}</code> permettra de renvoyer tous 
				les mots clés et le critère <code>{technique=oui}</code> 
				permet de sélectionner par type technique.',
	'mots_techniques_slogan' => 'Ajoute une spécialisation « technique » aux groupes de mots clés.',
	'mots_techniques_titre' => 'Mots techniques',
];
