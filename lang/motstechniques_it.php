<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/motstechniques?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_mots_cles_techniques' => 'Le parole chiave in questo gruppo verranno nascoste per impostazione predefinita nello spazio pubblico?',

	// I
	'info_mots_cles_techniques' => 'Gruppo di parole chiave tecniche:',
];
